const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors('*'))
app.use(express.json())

const routerEmp = require('./router/emp')
app.use('/emp', routerEmp)

app.listen(4000, '0.0.0.0', () => {
    console.log('server start at port 4000')
})
