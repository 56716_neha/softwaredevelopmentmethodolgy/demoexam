const mysql = require('mysql2')

//connection pool
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'manager',
    database: 'employee',
    waitForConnections:true,
    connectionLimit: 10,
    queueLimit:0,
})

module.exports = pool